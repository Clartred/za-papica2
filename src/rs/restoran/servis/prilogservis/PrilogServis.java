package rs.restoran.servis.prilogservis;

import rs.restoran.model.hrana.Obrok;
import rs.restoran.model.hrana.Prilog;

import java.util.concurrent.ThreadLocalRandom;

public class PrilogServis {
    private final int[] PRILOG_OPSEG_CENA = new int[]{20, 100};

    public int napraviCenu() {
        return ThreadLocalRandom.current().nextInt(PRILOG_OPSEG_CENA[0], PRILOG_OPSEG_CENA[1] + 1);
    }

    public void dodajPrilog(String nazivPriloga, Obrok obrok) {
        Prilog prilog = kreirajPrilog(nazivPriloga);
        obrok.getPrilozi().add(prilog);
        obrok.setUkupnaCena(obrok.getUkupnaCena() + prilog.getCena());
    }

    public Prilog kreirajPrilog(String nazivPriloga) {
        Prilog prilog = new Prilog(nazivPriloga);
        prilog.setCena(napraviCenu());
        System.out.println("Cena priloga " + prilog.getNaziv() + " je " + prilog.getCena() + " dinara.");
        return prilog;
    }
}
