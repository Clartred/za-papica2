package rs.restoran.servis;

import rs.restoran.model.Meni;
import rs.restoran.model.MeniStavka;
import rs.restoran.model.Narudzbina;
import rs.restoran.model.hrana.Obrok;
import rs.restoran.model.hrana.Prilog;
import rs.restoran.model.pice.Pice;
import rs.restoran.servis.obrokservis.PastaServis;
import rs.restoran.servis.obrokservis.PizzaServis;
import rs.restoran.servis.piceservis.GaziraniSokServis;
import rs.restoran.servis.piceservis.NegaziraniSokServis;
import rs.restoran.servis.piceservis.PiceServis;
import rs.restoran.servis.piceservis.VodaServis;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class RestoranServis {

    private PiceServis gaziraniSokServis;
    private PiceServis negaziraniSokServis;
    private PiceServis vodaServis;
    private PastaServis pastaServis;
    private PizzaServis pizzaServis;
    private List<Narudzbina> narudzbine = new ArrayList<>();


    public RestoranServis() {
        this.gaziraniSokServis = new GaziraniSokServis();
        this.negaziraniSokServis = new NegaziraniSokServis();
        this.vodaServis = new VodaServis();
        this.pizzaServis = new PizzaServis();
        this.pastaServis = new PastaServis();
    }

    public Narudzbina napraviNarudzbinu(int brojStola) {
        Narudzbina narudzbina = new Narudzbina(brojStola);
        narudzbine.add(narudzbina);
        return narudzbina;
    }

    public void izdajRacun(int brojStola) {
        Narudzbina narudzbina = nadjiNarudzbinuPoBrojuStola(brojStola);
        System.out.println("Racun za narudzbinu pod brojem " + narudzbina.getRedniBroj() + ", na stolu pod brojem " + narudzbina.getBrojStola());
        int racun = izracunajRacun(narudzbina);
        System.out.println("Vas racun je: " + racun + " dinara.");
    }

    public void naruciPice(String nazivPica, double zapremina, String vrstaPica, int brojStola) {
        Narudzbina narudzbina = nadjiNarudzbinuPoBrojuStola(brojStola);
        Pice pice;
        if (vrstaPica.equals("NEGAZIRANI_SOK")) {
            pice = naruciNegaziraniSok(nazivPica, zapremina);
        } else if (vrstaPica.equals("GAZIRANI_SOK")) {
            pice = naruciGaziraniSok(nazivPica, zapremina);
        } else {
            pice = naruciVodu(nazivPica, zapremina);
        }
        narudzbina.getPica().add(pice);
    }

    public void naruciObrok(String nazivHrane, String vrstaHrane, int brojStola) {
        Narudzbina narudzbina = nadjiNarudzbinuPoBrojuStola(brojStola);
        Obrok obrok;
        if (vrstaHrane.equals("PASTA")) {
            obrok = naruciPastu(nazivHrane);
        } else {
            obrok = naruciPizzu(nazivHrane);
        }
        narudzbina.getObroci().add(obrok);
    }

    public Narudzbina nadjiNarudzbinuPoBrojuStola(int brojStola) {
        for (Narudzbina narudzbina : narudzbine) {
            if (narudzbina.getBrojStola() == brojStola) {
                return narudzbina;
            }
        }
        return napraviNarudzbinu(brojStola);
    }

    public void izlistajSveNarudzbine() {
        for (Narudzbina narudzbina : narudzbine) {
            System.out.println("Narudzbina broj: " + narudzbina.getRedniBroj());
            System.out.println("Na stolu: " + narudzbina.getBrojStola());
        }
    }

    private Obrok naruciPastu(String nazivPaste) {
        return pastaServis.naruciObrok(nazivPaste);
    }

    private Obrok naruciPizzu(String nazivPizze) {
        return pizzaServis.naruciObrok(nazivPizze);
    }

    private Pice naruciGaziraniSok(String nazivPica, double zapremina) {
        return gaziraniSokServis.naruciPice(nazivPica, zapremina);
    }

    private Pice naruciVodu(String nazivPica, double zapremina) {
        return vodaServis.naruciPice(nazivPica, zapremina);
    }

    private Pice naruciNegaziraniSok(String nazivPica, double zapremina) {
        return negaziraniSokServis.naruciPice(nazivPica, zapremina);
    }

    private int izracunajRacun(Narudzbina narudzbina) {
        int cenaHrane = izracunajCenuHrane(narudzbina);
        int cenaPica = izracunajCenuPica(narudzbina);
        System.out.println("Ukupna cena hrane: " + cenaHrane + " dinara.");
        System.out.println("Ukupna cena pica: " + cenaPica + " dinara.");
        return cenaHrane + cenaPica;
    }

    private int izracunajCenuHrane(Narudzbina narudzbina) {
        int racun = 0;
        System.out.println("Hrana koju ste narucili: ");
        for (Obrok obrok : narudzbina.getObroci()) {
            racun += obrok.getUkupnaCena();
            System.out.println(obrok.getNaziv());
            if (obrok.getPrilozi().size() > 0) {
                System.out.println("Sa prilozima: ");
                for (Prilog prilog : obrok.getPrilozi()) {
                    System.out.println(prilog.getNaziv() + "\t\t\t" + prilog.getCena());
                }
            }
            System.out.println("Ukupna cena obroka je " + obrok.getUkupnaCena());
        }
        return racun;
    }

    private int izracunajCenuPica(Narudzbina narudzbina) {
        int racun = 0;
        System.out.println("Pica koja ste narucili: ");
        for (Pice pice : narudzbina.getPica()) {
            racun += pice.getCena();
            System.out.println(pice.getNaziv() + " u kolicini " + pice.getZapremina() + "\t\t\t" + pice.getCena());
        }
        return racun;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        MeniServis meniServis = new MeniServis();
        meniServis.kreirajMeni();
        Meni meni = Meni.dajMeniObjekat();
        int brojStola = ThreadLocalRandom.current().nextInt(1, 50 + 1);
        System.out.println("Dobar dan. Dobrodosli u restoran: \n'NE SLUZIMO PIJANE GOSTE!'\n");
        System.out.println("Vas broj stola je: " + brojStola + ", mozete sesti.");
        System.out.println("Da bi ste narucili odredjeni predmet, ukucajte broj koji je pored naziva:\n");
        meniServis.izlistajCeoMeni();
        List<Integer> listaBrojeva = new ArrayList<>();
        System.out.println("Molimo vas unesite listu brojeva sa narudzbinama. \n Ukoliko zelite da ponovo pogledate meni, pristine broj 0. \n Ukoliko ste zavrsili sa narudzbinom, pritistnite broj -1");
        while (true) {

            System.out.println("");
            try {
                int input = Integer.parseInt(scanner.nextLine());

                if (input < -2 || input > meni.getBrojStavki()) {
                    System.out.println("Izabrali ste stavku koja ne postoji!");
                } else {
                    if (input == 0) {
                        meniServis.izlistajCeoMeni();
                    } else if (input == -1) {
                        break;
                    } else {
                        System.out.println("Uneli ste stavku pod brojem: " + input);
                        listaBrojeva.add(input);
                    }
                }
            } catch(NumberFormatException exception){
                System.out.println("MORATE UNETI SAMO BROJ!!!!!");
            }
        }

        System.out.println("Stavke koje ste izabrali su: ");
        for (Integer integer : listaBrojeva) {
            MeniStavka stavka = meniServis.nadjiStavkuPoBrojuStavke(integer);
            if (stavka != null) {
                System.out.println(stavka.getNaziv());
                String vrstaStavke = stavka.getVrstaStavke();
                if (vrstaStavke.equals("PIZZA") || vrstaStavke.equals("PASTA")) {
                    naruciObrok(stavka.getNaziv(), vrstaStavke, brojStola);
                } else if (vrstaStavke.equals("PRILOG")) {
                    System.out.println("papicu, dodaj za narucivanje priloga i nakaci ga na obrok. ");
                } else if (vrstaStavke.equals("VODA") || vrstaStavke.equals("GAZIRANI_SOK") || vrstaStavke.equals("NEGAZIRANI_SOK")) {
                    double zapremina = ThreadLocalRandom.current().nextInt(1, 100);
                    naruciPice(stavka.getNaziv(), zapremina, vrstaStavke, brojStola);
                }
            }
        }
        try {
            TimeUnit.SECONDS.sleep(10);
            System.out.println("\n\n\n\n");
            System.out.println("Nadamo se da je obrok bio prijatan");
            System.out.println("Vas racun je: ");
            izdajRacun(brojStola);
        } catch (InterruptedException exception) {
            System.out.println("Treba i exception handling da stavis. to stavi tamo gde korisnik unese da hce prilog pre nego sto unese neki obrok. GG!!!! ");
        }

    }

}
