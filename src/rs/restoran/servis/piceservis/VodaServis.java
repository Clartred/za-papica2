package rs.restoran.servis.piceservis;

import rs.restoran.model.pice.GaziraniSok;
import rs.restoran.model.pice.Pice;

import java.util.concurrent.ThreadLocalRandom;

public class VodaServis implements PiceServis {

    @Override
    public int napraviCenu() {
        return ThreadLocalRandom.current().nextInt(PICE_OPSEG_CENA[0], PICE_OPSEG_CENA[1] + 1);
    }

    @Override
    public Pice naruciPice(String nazivPica, double zapremina) {
        return kreirajPice(nazivPica, zapremina);
    }

    public Pice kreirajPice(String nazivPica, double zapremina) {
        Pice pice = new GaziraniSok(nazivPica, zapremina);
        pice.setCena(napraviCenu());
        System.out.println("Cena vode " + pice.getNaziv() + " je " + pice.getCena() + " dinara.");
        return pice;
    }
}