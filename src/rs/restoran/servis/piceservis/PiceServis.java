package rs.restoran.servis.piceservis;

import rs.restoran.model.pice.Pice;

public interface PiceServis {

    final int[] PICE_OPSEG_CENA = new int[]{150, 500};

    int napraviCenu();

    Pice kreirajPice(String nazivPica, double zapremina);
    Pice naruciPice(String nazivPica, double zapremina);
}
