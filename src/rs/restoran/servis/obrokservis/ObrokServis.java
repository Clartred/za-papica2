package rs.restoran.servis.obrokservis;

import rs.restoran.model.hrana.Obrok;
import rs.restoran.model.hrana.Prilog;

import java.util.List;

public interface ObrokServis {

    final int[] OBROK_OPSEG_CENA = new int[]{300, 600};

    public int napraviPocetnuCenu();

    Obrok naruciObrok(String nazivObroka);
}
