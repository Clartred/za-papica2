package rs.restoran.servis.obrokservis;

import rs.restoran.model.hrana.Obrok;
import rs.restoran.model.hrana.Pasta;

import java.util.concurrent.ThreadLocalRandom;

public class PastaServis implements ObrokServis {

    @Override
    public int napraviPocetnuCenu() {
        return ThreadLocalRandom.current().nextInt(OBROK_OPSEG_CENA[0], OBROK_OPSEG_CENA[1] + 1);
    }

    public Obrok naruciObrok(String nazivObroka) {
        Obrok pasta = kreirajObrok(nazivObroka);
        return pasta;
    }

    public Obrok kreirajObrok(String nazivObroka) {
        Obrok obrok = new Pasta(nazivObroka);
        int cena = napraviPocetnuCenu();
        obrok.setPocetnaCena(cena);
        obrok.setUkupnaCena(cena);
        System.out.println("Pocetna cena paste " + obrok.getNaziv() + " je " + obrok.getPocetnaCena() + " dinara.");
        return obrok;
    }

}
