package rs.restoran.servis.obrokservis;

import rs.restoran.model.hrana.Obrok;
import rs.restoran.model.hrana.Pizza;

import java.util.concurrent.ThreadLocalRandom;

public class PizzaServis implements ObrokServis {

    @Override
    public int napraviPocetnuCenu() {
        return ThreadLocalRandom.current().nextInt(OBROK_OPSEG_CENA[0], OBROK_OPSEG_CENA[1] + 1);
    }

    public Obrok naruciObrok(String nazivObroka) {
        Obrok pizza = kreirajObrok(nazivObroka);
        return pizza;
    }

    public Obrok kreirajObrok(String nazivObroka) {
        Obrok obrok = new Pizza(nazivObroka);
        int cena = napraviPocetnuCenu();
        obrok.setPocetnaCena(cena);
        obrok.setUkupnaCena(cena);
        System.out.println("Pocetna cena pizze " + obrok.getNaziv() + " je " + obrok.getPocetnaCena() + " dinara.");
        return obrok;
    }

}
