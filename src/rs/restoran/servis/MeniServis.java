package rs.restoran.servis;

import rs.restoran.model.Meni;
import rs.restoran.model.MeniStavka;

public class MeniServis {

    Meni meni = Meni.dajMeniObjekat();
    String pastaMenu[] = new String[]{"Pene", "Talatele", "Fusili", "Rigate"};
    String pizzaMenu[] = new String[]{"Capricosa", "Margarita", "Vojvodjanka", "4 sezone"};
    String priloziMenu[] = new String[]{"Kecap", "Majonez", "Masline", "Pavlaka"};
    String gaziraniSokoviMenu[] = new String[]{"Koka Kola", "Sprite", "Fanta"};
    String negaziraniSokoviMenu[] = new String[]{"Breskva", "Jabuka", "Kajsija", "Malina"};
    String vodaMenu[] = new String[]{"Voda Voda", "Aqua Viva", "Rosa"};

    public void kreirajMeni() {
        for (String pasta : pastaMenu) {
            dodajPastu(pasta);
        }
        for (String pizza : pizzaMenu) {
            dodajPizzu(pizza);
        }
        for (String prilog : priloziMenu) {
            dodajPrilog(prilog);
        }
        for (String gaziraniSok : gaziraniSokoviMenu) {
            dodajGaziraniSok(gaziraniSok);
        }
        for (String negaziraniSok : negaziraniSokoviMenu) {
            dodajneGaziraniSok(negaziraniSok);
        }
        for (String voda : vodaMenu) {
            dodajVodu(voda);
        }
    }

    public void izlistajCeoMeni() {

        String paste = "";
        String pizze = "";
        String prilozi = "";
        String gaziraniSokovi = "";
        String negaziraniSokovi = "";
        String vode = "";

        System.out.println("\nPaste: \n");
        for (MeniStavka pasta : meni.getPastaMenu()) {
            paste += pasta.toString() + "   ";
        }
        System.out.println(paste);
        System.out.println("\nPizze: \n");
        for (MeniStavka pizza : meni.getPizzaMenu()) {
            pizze += pizza.toString() + "   ";
        }
        System.out.println(pizze);

        System.out.println("\nPrilozi: \n");
        for (MeniStavka prilogs : meni.getPrilogMenu()) {
            prilozi += prilogs.toString() + "    ";
        }
        System.out.println(prilozi);
        System.out.println("\nGazirani sokovi: \n");
        for (MeniStavka gaziraniSokovs : meni.getGaziraniSokoviMenu()) {
            gaziraniSokovi += gaziraniSokovs.toString() + "    ";
        }
        System.out.println(gaziraniSokovi);
        System.out.println("\nNegazirani sokovi:\n ");
        for (MeniStavka negaziraniSokovs : meni.getNegaziraniSokoviMenu()) {
            negaziraniSokovi += negaziraniSokovs.toString() + "     ";
        }
        System.out.println(negaziraniSokovi);
        System.out.println("\nVode:\n ");
        for (MeniStavka vods : meni.getVodaMenu()) {
            vode += vods.toString() + "     ";
        }
        System.out.println(vode);
        System.out.println("\n\n");
    }

    public MeniStavka nadjiStavkuPoBrojuStavke(int brojStavke) {
        for (MeniStavka pasta : meni.getPastaMenu()) {
            if (pasta.getRedniBroj() == brojStavke) {
                return pasta;
            }
        }
        for (MeniStavka pizza : meni.getPizzaMenu()) {
            if (pizza.getRedniBroj() == brojStavke) {
                return pizza;
            }
        }
        for (MeniStavka prilozi : meni.getPrilogMenu()) {
            if (prilozi.getRedniBroj() == brojStavke) {
                return prilozi;
            }
        }
        for (MeniStavka gaziraniSokovi : meni.getGaziraniSokoviMenu()) {
            if (gaziraniSokovi.getRedniBroj() == brojStavke) {
                return gaziraniSokovi;
            }
        }
        for (MeniStavka negaziraniSokovi : meni.getNegaziraniSokoviMenu()) {
            if (negaziraniSokovi.getRedniBroj() == brojStavke) {
                return negaziraniSokovi;
            }
        }
        for (MeniStavka vode : meni.getVodaMenu()) {
            if (vode.getRedniBroj() == brojStavke) {
                return vode;
            }
        }
        return null;
    }

    public void dodajGaziraniSok(String naziv) {
        meni.getGaziraniSokoviMenu().add(napraviMeniStavku(naziv,"GAZIRANI_SOK"));
    }

    public void dodajneGaziraniSok(String naziv) {
        meni.getNegaziraniSokoviMenu().add(napraviMeniStavku(naziv, "NEGAZIRANI_SOK"));
    }

    public void dodajVodu(String naziv) {
        meni.getVodaMenu().add(napraviMeniStavku(naziv, "VODA"));
    }

    public void dodajPastu(String naziv) {
        meni.getPastaMenu().add(napraviMeniStavku(naziv, "PASTA"));
    }

    public void dodajPrilog(String naziv) {
        meni.getPrilogMenu().add(napraviMeniStavku(naziv, "PRILOG"));
    }

    public void dodajPizzu(String naziv) {
        meni.getPizzaMenu().add(napraviMeniStavku(naziv, "PIZZA"));
    }

    public MeniStavka napraviMeniStavku(String naziv, String vrstaStavke) {
        meni.povecajBrojStakvi();
        MeniStavka meniStavka = new MeniStavka(meni.getBrojStavki(), naziv, vrstaStavke);
        return meniStavka;
    }


}
