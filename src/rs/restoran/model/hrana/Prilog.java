package rs.restoran.model.hrana;

import java.util.concurrent.ThreadLocalRandom;

public class Prilog {

    private String naziv;
    private int cena;

    public Prilog(String naziv) {
        this.naziv = naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

}
