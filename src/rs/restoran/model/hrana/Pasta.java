package rs.restoran.model.hrana;

import rs.restoran.model.hrana.Obrok;

public class Pasta extends Obrok {

    public Pasta(String naziv) {
        super(naziv);
    }
}
