package rs.restoran.model.hrana;

import java.util.ArrayList;
import java.util.List;

public abstract class Obrok {

    private String naziv;
    private int pocetnaCena;
    private int ukupnaCena;
    private List<Prilog> prilozi = new ArrayList<>();

    public Obrok(String naziv) {
        this.naziv = naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getPocetnaCena() {
        return pocetnaCena;
    }

    public void setPocetnaCena(int pocetnaCena) {
        this.pocetnaCena = pocetnaCena;
    }

    public int getUkupnaCena() {
        return ukupnaCena;
    }

    public void setUkupnaCena(int ukupnaCena) {
        this.ukupnaCena = ukupnaCena;
    }

    public List<Prilog> getPrilozi() {
        return prilozi;
    }

    public void setPrilozi(List<Prilog> prilozi) {
        this.prilozi = prilozi;
    }
}
