package rs.restoran.model;

import rs.restoran.model.hrana.Obrok;
import rs.restoran.model.pice.Pice;

import java.util.ArrayList;
import java.util.List;

public class Narudzbina {

    private int redniBroj;
    private int brojStola;
    private List<Obrok> obroci = new ArrayList<>();
    private List<Pice> pica = new ArrayList<>();

    public Narudzbina(int brojStola) {
        this.brojStola = brojStola;
    }

    public int getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(int redniBroj) {
        this.redniBroj = redniBroj;
    }

    public int getBrojStola() {
        return brojStola;
    }

    public void setBrojStola(int brojStola) {
        this.brojStola = brojStola;
    }

    public List<Obrok> getObroci() {
        return obroci;
    }

    public void setObroci(List<Obrok> obroci) {
        this.obroci = obroci;
    }

    public List<Pice> getPica() {
        return pica;
    }

    public void setPica(List<Pice> pica) {
        this.pica = pica;
    }
}
