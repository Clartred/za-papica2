package rs.restoran.model;

import java.util.ArrayList;
import java.util.List;

public class Sto {

    private int brojStola;
    private List<Narudzbina> narudzbine = new ArrayList<>();

    public Sto(int brojStola) {
        this.brojStola = brojStola;
    }

    public int getBrojStola() {
        return brojStola;
    }

    public void setBrojStola(int brojStola) {
        this.brojStola = brojStola;
    }

    public List<Narudzbina> getNarudzbine() {
        return narudzbine;
    }

    public void setNarudzbine(List<Narudzbina> narudzbine) {
        this.narudzbine = narudzbine;
    }
}
