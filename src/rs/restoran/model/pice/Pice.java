package rs.restoran.model.pice;

import java.util.concurrent.ThreadLocalRandom;

public abstract class Pice {

    private String naziv;
    private int cena;
    private double zapremina;

    public Pice(String naziv, double zapremina) {
        this.naziv = naziv;
        this.zapremina = zapremina;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public double getZapremina() {
        return zapremina;
    }

    public void setZapremina(double zapremina) {
        this.zapremina = zapremina;
    }
}
