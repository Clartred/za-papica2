package rs.restoran.model;

public class MeniStavka {

    public MeniStavka(int redniBroj, String naziv, String vrstaStavke) {
        this.redniBroj = redniBroj;
        this.naziv = naziv;
        this.vrstaStavke = vrstaStavke;
    }

    private int redniBroj;
    private String naziv;
    private String vrstaStavke;

    public int getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(int redniBroj) {
        this.redniBroj = redniBroj;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public String toString() {
        return naziv + "\t\t\t " + redniBroj;
    }

    public String getVrstaStavke() {
        return vrstaStavke;
    }

    public void setVrstaStavke(String vrstaStavke) {
        this.vrstaStavke = vrstaStavke;
    }
}
