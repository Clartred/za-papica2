package rs.restoran.model;

import java.util.ArrayList;
import java.util.List;

public class Meni {

    private List<MeniStavka> pastaMenu = new ArrayList<>();
    private List<MeniStavka> pizzaMenu = new ArrayList<>();
    private List<MeniStavka> prilogMenu = new ArrayList<>();
    private List<MeniStavka> gaziraniSokoviMenu = new ArrayList<>();
    private List<MeniStavka> negaziraniSokoviMenu = new ArrayList<>();
    private List<MeniStavka> vodaMenu = new ArrayList<>();
    private static Meni meni;
    private int brojStavki = 0;

    private Meni() {
    }

    public static Meni dajMeniObjekat() {
        if (meni == null) {
            meni = new Meni();
        }
        return meni;
    }

    public void povecajBrojStakvi() {
        brojStavki++;
    }

    public List<MeniStavka> getPastaMenu() {
        return pastaMenu;
    }

    public void setPastaMenu(List<MeniStavka> pastaMenu) {
        this.pastaMenu = pastaMenu;
    }

    public List<MeniStavka> getPizzaMenu() {
        return pizzaMenu;
    }

    public void setPizzaMenu(List<MeniStavka> pizzaMenu) {
        this.pizzaMenu = pizzaMenu;
    }

    public List<MeniStavka> getGaziraniSokoviMenu() {
        return gaziraniSokoviMenu;
    }

    public void setGaziraniSokoviMenu(List<MeniStavka> gaziraniSokoviMenu) {
        this.gaziraniSokoviMenu = gaziraniSokoviMenu;
    }

    public List<MeniStavka> getNegaziraniSokoviMenu() {
        return negaziraniSokoviMenu;
    }

    public void setNegaziraniSokoviMenu(List<MeniStavka> negaziraniSokoviMenu) {
        this.negaziraniSokoviMenu = negaziraniSokoviMenu;
    }

    public List<MeniStavka> getVodaMenu() {
        return vodaMenu;
    }

    public void setVodaMenu(List<MeniStavka> vodaMenu) {
        this.vodaMenu = vodaMenu;
    }

    public int getBrojStavki() {
        return brojStavki;
    }

    public void setBrojStavki(int brojStavki) {
        this.brojStavki = brojStavki;
    }

    public List<MeniStavka> getPrilogMenu() {
        return prilogMenu;
    }

    public void setPrilogMenu(List<MeniStavka> prilogMenu) {
        this.prilogMenu = prilogMenu;
    }
}
