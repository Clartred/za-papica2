package rs.restoran;

import rs.restoran.servis.RestoranServis;

public class Main {

    public static void main(String[] args) {

        RestoranServis restoranServis = new RestoranServis();
        restoranServis.start();
    }
}